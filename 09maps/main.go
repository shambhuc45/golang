package main

import "fmt"

func main() {

	Info := map[string]string{}
	Info["name"] = "Shambhu Chaudhary"
	Info["address"] = "Kathmandu"
	Info["gender"] = "male"

	// accessing the map
	fmt.Println("Name is :", Info["name"])
	fmt.Println("Address is :", Info["address"])
	fmt.Println("Gender is :", Info["gender"])

	// also u can delete the key
	
	fmt.Println(Info)
	delete(Info,"gender")
	fmt.Println(Info)

	//loop in GoLang. the below loop is similar to foreach loop in Js

	for key,value:=range Info{
		fmt.Printf("For key %v,value is %v \n", key, value)
	}
}