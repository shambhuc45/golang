package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Enter Your age:")
	ageInput,_:=reader.ReadString('\n')

	var result string

	age,err := strconv.ParseFloat(strings.TrimSpace(ageInput),64)

	if err != nil {
		fmt.Println(err)	
		}else{
			if age <18{
				result = "Cannot enter the bar"
			}else{
				result = "Can enter the bar"
			}
		}
	
		fmt.Println(result)
}