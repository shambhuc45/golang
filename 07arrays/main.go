package main

import "fmt"

func main() {
	var Fruits [4]string

	Fruits[0]="apple"
	Fruits[1]="banana"
	Fruits[2]="orange"
	fmt.Println(Fruits)
	fmt.Println(len(Fruits)) // 4



	var vegies = [3] string {"potato","aalu"}
	fmt.Println(vegies)
	fmt.Println(len(vegies)) // 3
	fmt.Printf("Type of vegies is: %T", vegies)


}


// so declared sized array is created even if you provide only one item in the array 
// Arrays are replaced by slices in Golang