package main

import "fmt"

func increment(x int) {
    x++
}
func main() {
	var myNumber = 23

	var pointerToMyNumber = &myNumber                  // this points to the address of the myNumber.
	fmt.Println("value of pointer", pointerToMyNumber) // this will show the address

	fmt.Println("value of pointer", *pointerToMyNumber) // this will show the actual value i.e.23
//output is:
// value of pointer 0xc000096068
// value of pointer 23

// we can also perform operations using pointer:
*pointerToMyNumber = *pointerToMyNumber +3
fmt.Println("Added value to pointer and the original variable are:",*pointerToMyNumber, myNumber)

num := 10
increment(num)
fmt.Println(num) 
}


