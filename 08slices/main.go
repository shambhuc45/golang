package main

import (
	"fmt"
	"sort"
)

func main() {

	var names = []string{"shambhu", "kamal", "bishnu", "manoj"}
	fmt.Println(names)
	fmt.Println(len(names))
	fmt.Printf("Type of names is: %T \n", names)


	// Adding items to slices can be done by using append 
	names = append(names, "krity", "nirva")
	fmt.Println(names)

	names=append(names[1:3])  // this simply says that slice the array(slice) starting from 1 index to index 3

	fmt.Println(names)


	// Sorting slices items 

	score:= []int {12,19,9,75}
	fmt.Println(score)
	fmt.Println("Are the score sorted ?",sort.IntsAreSorted(score))

	sort.Ints(score)
	fmt.Println(score)
	fmt.Println("Are the score sorted ?",sort.IntsAreSorted(score))


	// removing value in slices 
	courses := []string{"react","node","golang","express"}
	// lets remove the golang from the slice 
	var index = 2 // golang is at index 2
	courses = append(courses[:index],courses[index+1:]...)
	fmt.Println(courses)
}